package io.rasi.bank;

import com.fasterxml.jackson.annotation.JsonCreator;

public class TransferId extends BaseId {

    TransferId(long id) {
        super(id);
    }

    @JsonCreator
    public static TransferId valueOf(String str) {
        return valueOf(str, TransferId::new);
    }
}
