package io.rasi.bank;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountView {

    @JsonProperty
    public final AccountId id;
    @JsonProperty
    public final Money balance;

    @JsonCreator
    public AccountView(@JsonProperty("id") AccountId id, @JsonProperty("balance") Money balance) {
        this.id = id;
        this.balance = balance;
    }
}
