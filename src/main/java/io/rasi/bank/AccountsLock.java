package io.rasi.bank;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * This class encapsulates ReadWriteLocks of multiple accounts.
 * Locks are always sorted in predictable order (by account ids) to avoid deadlocks.
 */
class AccountsLock {

    private final List<Lock> locks;

    AccountsLock(Type type, Account... accounts) {
        locks = Stream.of(accounts)
                .sorted(Comparator.comparing(account -> account.id))
                .map(account -> account.lock)
                .map(type.getLock)
                .collect(toList());
    }

    void lock() {
        locks.forEach(Lock::lock);
    }

    void unlock() {
        locks.forEach(Lock::unlock);
    }

    enum Type {
        READ(ReadWriteLock::readLock),
        WRITE(ReadWriteLock::writeLock);

        final Function<ReadWriteLock, Lock> getLock;

        Type(Function<ReadWriteLock, Lock> getLock) {
            this.getLock = getLock;
        }
    }
}
