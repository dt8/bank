package io.rasi.bank;

import com.fasterxml.jackson.annotation.JsonValue;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.function.Function;

class BaseId implements Comparable<BaseId> {

    public final long id;

    BaseId(long id) {
        if (id < 1) {
            throw new IllegalArgumentException("Id must be a positive number");
        }
        this.id = id;
    }

    static <T extends BaseId> T valueOf(String str, Function<Long, T> constructor) {
        return constructor.apply(Long.parseLong(str));
    }

    @JsonValue
    private String jsonValue() {
        return String.valueOf(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseId baseId = (BaseId) o;
        return id == baseId.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "id=" + id +
                '}';
    }

    @Override
    public int compareTo(@NotNull BaseId o) {
        return Long.compare(id, o.id);
    }
}
