package io.rasi.bank;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * This class is an infinite log of items.
 * It allows for prepending new ones and iterating over currently added via stream() method.
 *
 * Synchronization happens only in prepend() method.
 * Stream returned from stream() method is a view of items stored by log at the moment method mas called.
 */
class ItemsLog<T> {

    private volatile Item<T> head = null;

    synchronized void prepend(T value) {
        head = new Item<>(value, head);
    }

    Stream<T> stream() {
        return StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(new ItemsIterator<>(head), Spliterator.ORDERED),
                false
        );
    }

    private static final class Item<T> {

        final T value;
        final Item<T> next;

        Item(T value, Item<T> next) {
            this.value = value;
            this.next = next;
        }
    }

    private static final class ItemsIterator<T> implements Iterator<T> {

        Item<T> nextItem;

        ItemsIterator(Item<T> nextItem) {
            this.nextItem = nextItem;
        }

        @Override
        public boolean hasNext() {
            return nextItem != null;
        }

        @Override
        public T next() {
            T next = nextItem.value;
            nextItem = nextItem.next;
            return next;
        }
    }
}
