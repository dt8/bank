package io.rasi.bank;

import io.rasi.bank.rest.BankWebApp;

public class Main {

    public static void main(String[] args) {
        Bank bank = new Bank();
        BankWebApp bankWebApp = new BankWebApp(8080, bank);
        bankWebApp.start();
    }
}
