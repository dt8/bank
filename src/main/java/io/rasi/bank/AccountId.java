package io.rasi.bank;

import com.fasterxml.jackson.annotation.JsonCreator;

public class AccountId extends BaseId {

    AccountId(long id) {
        super(id);
    }

    @JsonCreator
    public static AccountId valueOf(String str) {
        return valueOf(str, AccountId::new);
    }
}
