package io.rasi.bank;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.stream.Collectors.toList;

class TransfersRepository {

    private final AtomicLong transferIdsSeq = new AtomicLong(0);
    private final ItemsLog<Transfer> transfersItemsLog = new ItemsLog<>();

    Transfer create(AccountId debitedAccount, AccountId creditedAccount, Money amount) {
        TransferId id = new TransferId(transferIdsSeq.incrementAndGet());
        Transfer transfer = new Transfer(id, amount, debitedAccount, creditedAccount);
        transfersItemsLog.prepend(transfer);
        return transfer;
    }

    List<Transfer> getForAccount(AccountId accountId) {
        return transfersItemsLog.stream()
                .filter(transfer -> transfer.isParty(accountId))
                .collect(toList());
    }
}
