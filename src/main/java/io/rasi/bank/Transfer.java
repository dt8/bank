package io.rasi.bank;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Transfer {

    @JsonProperty
    public final TransferId id;
    @JsonProperty
    public final Money amount;
    @JsonProperty
    public final AccountId debitedAccount;
    @JsonProperty
    public final AccountId creditedAccount;

    @JsonCreator
    public Transfer(@JsonProperty("id") TransferId id,
                    @JsonProperty("amount") Money amount,
                    @JsonProperty("debitedAccount") AccountId debitedAccount,
                    @JsonProperty("creditedAccount") AccountId creditedAccount) {
        this.id = id;
        this.amount = amount;
        this.debitedAccount = debitedAccount;
        this.creditedAccount = creditedAccount;
    }

    public boolean isParty(AccountId accountId) {
        return accountId.equals(debitedAccount) || accountId.equals(creditedAccount);
    }

    @Override
    public String toString() {
        return "Transfer{" +
                "id=" + id +
                ", amount=" + amount +
                ", debitedAccount=" + debitedAccount +
                ", creditedAccount=" + creditedAccount +
                '}';
    }
}
