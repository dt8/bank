package io.rasi.bank;

import java.math.BigDecimal;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public class Money {

    public static final Money ZERO = new Money(new BigDecimal("0.00"));

    private final BigDecimal amount;

    private Money(BigDecimal amount) {
        if (amount.scale() > 2) {
            throw new IllegalArgumentException("Money must be represented with at most 2 decimal places");
        }
        // rounding mode not needed as we require values to have the scale set to at most 2
        this.amount = amount.setScale(2);
    }

    @JsonCreator
    public static Money valueOf(String str) {
        return new Money(new BigDecimal(str));
    }

    public static Money valueOf(int amount) {
        return new Money(new BigDecimal(amount));
    }

    public Money add(Money money) {
        return new Money(amount.add(money.amount));
    }

    public Money subtract(Money money) {
        return new Money(amount.subtract(money.amount));
    }

    public boolean greaterThat(Money money) {
        return amount.compareTo(money.amount) > 0;
    }

    @JsonValue
    public String jsonValue() {
        return amount.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Money money = (Money) o;
        return Objects.equals(amount, money.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount);
    }

    @Override
    public String toString() {
        return "Money{" +
                "amount=" + amount +
                '}';
    }
}
