package io.rasi.bank;

import io.rasi.bank.exceptions.InvalidTransferRecipientException;
import io.rasi.bank.exceptions.NotEnoughFundsException;
import io.rasi.bank.exceptions.InvalidMoneyValueException;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class Account {

    final AccountId id;
    final ReadWriteLock lock;
    private Money balance;

    Account(AccountId id, Money balance) {
        this.id = id;
        this.balance = balance;
        this.lock = new ReentrantReadWriteLock();
    }

    void transfer(Account recipient, Money amount)
            throws InvalidMoneyValueException, NotEnoughFundsException, InvalidTransferRecipientException {

        if (!amount.greaterThat(Money.ZERO)) {
            throw new InvalidMoneyValueException();
        }

        if (amount.greaterThat(balance)) {
            throw new NotEnoughFundsException();
        }

        if (id.equals(recipient.id)) {
            throw new InvalidTransferRecipientException();
        }

        this.balance = this.balance.subtract(amount);
        recipient.balance = recipient.balance.add(amount);
    }

    Money balance() {
        return balance;
    }
}
