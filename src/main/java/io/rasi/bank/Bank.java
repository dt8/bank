package io.rasi.bank;

import io.rasi.bank.exceptions.AccountNotFoundException;
import io.rasi.bank.exceptions.InvalidTransferRecipientException;
import io.rasi.bank.exceptions.NotEnoughFundsException;
import io.rasi.bank.exceptions.InvalidMoneyValueException;

import java.util.List;

import static io.rasi.bank.AccountsLock.Type.READ;
import static io.rasi.bank.AccountsLock.Type.WRITE;

public class Bank {

    private final AccountsRepository accountsRepository = new AccountsRepository();
    private final TransfersRepository transfersRepository = new TransfersRepository();

    public AccountView createAccount(Money balance) throws InvalidMoneyValueException {
        if (Money.ZERO.greaterThat(balance)) {
            throw new InvalidMoneyValueException();
        }

        Account account = accountsRepository.create(balance);
        return new AccountView(account.id, account.balance());
    }

    public Transfer transfer(AccountId debitedAccountId, AccountId creditedAccountId, Money amount)
            throws AccountNotFoundException, NotEnoughFundsException, InvalidMoneyValueException, InvalidTransferRecipientException {

        Account debitedAccount = accountsRepository.get(debitedAccountId);
        Account creditedAccount = getRecipient(creditedAccountId);

        AccountsLock lock = new AccountsLock(WRITE, debitedAccount, creditedAccount);
        lock.lock();

        try {
            debitedAccount.transfer(creditedAccount, amount);
            return transfersRepository.create(debitedAccountId, creditedAccountId, amount);
        } finally {
            lock.unlock();
        }
    }

    private Account getRecipient(AccountId id) throws InvalidTransferRecipientException {
        try {
            return accountsRepository.get(id);
        } catch (AccountNotFoundException e) {
            throw new InvalidTransferRecipientException();
        }
    }

    public AccountView getAccount(AccountId id) throws AccountNotFoundException {
        Account account = accountsRepository.get(id);

        AccountsLock lock = new AccountsLock(READ, account);
        lock.lock();

        try {
            return new AccountView(account.id, account.balance());
        } finally {
            lock.unlock();
        }
    }

    public List<Transfer> getTransfers(AccountId id) throws AccountNotFoundException {
        Account account = accountsRepository.get(id);

        AccountsLock lock = new AccountsLock(READ, account);
        lock.lock();

        try {
            return transfersRepository.getForAccount(id);
        } finally {
            lock.unlock();
        }
    }
}
