package io.rasi.bank.rest;

import io.javalin.Javalin;
import io.javalin.core.validation.JavalinValidation;
import io.javalin.http.ExceptionHandler;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.rasi.bank.AccountId;
import io.rasi.bank.Bank;
import io.rasi.bank.exceptions.AccountNotFoundException;
import io.rasi.bank.exceptions.InvalidMoneyValueException;
import io.rasi.bank.exceptions.InvalidTransferRecipientException;
import io.rasi.bank.exceptions.NotEnoughFundsException;
import io.swagger.v3.oas.models.info.Info;

public class BankWebApp {

    private final int port;
    private final Javalin webApp;

    public BankWebApp(Bank bank) {
        this(0, bank);
    }

    public BankWebApp(int port, Bank bank) {
        this.port = port;
        this.webApp = Javalin.create(config -> config.registerPlugin(new OpenApiPlugin(openApiOptions())));

        BankResource resource = new BankResource(bank);
        this.webApp.post("/accounts", resource::create);
        this.webApp.get("/accounts/:id", resource::get);
        this.webApp.post("/accounts/:id/transfers", resource::transfer);
        this.webApp.get("/accounts/:id/transfers", resource::getTransfers);

        this.webApp.exception(AccountNotFoundException.class, statusCode(404));
        this.webApp.exception(InvalidTransferRecipientException.class, statusCode(400));
        this.webApp.exception(InvalidMoneyValueException.class, statusCode(400));
        this.webApp.exception(NotEnoughFundsException.class, statusCode(400));

        JavalinValidation.register(AccountId.class, AccountId::valueOf);
    }

    private static OpenApiOptions openApiOptions() {
        return new OpenApiOptions(new Info().description("The Bank"))
                .activateAnnotationScanningFor("io.rasi.bank.rest")
                .path("/api-docs")
                .swagger(new SwaggerOptions("/"));
    }

    private static <T extends Exception> ExceptionHandler<T> statusCode(int statusCode) {
        return (exception, ctx) -> ctx.status(statusCode).result(exception.getClass().getSimpleName());
    }

    public void start() {
        webApp.start(port);
    }

    public int port() {
        return webApp.port();
    }

    public void stop() {
        webApp.stop();
    }
}
