package io.rasi.bank.rest;

import io.javalin.http.Context;
import io.javalin.plugin.openapi.annotations.*;
import io.rasi.bank.AccountId;
import io.rasi.bank.AccountView;
import io.rasi.bank.Bank;
import io.rasi.bank.Transfer;
import io.rasi.bank.exceptions.AccountNotFoundException;
import io.rasi.bank.exceptions.InvalidMoneyValueException;
import io.rasi.bank.exceptions.InvalidTransferRecipientException;
import io.rasi.bank.exceptions.NotEnoughFundsException;

import java.util.List;

import static io.javalin.plugin.openapi.annotations.HttpMethod.GET;
import static io.javalin.plugin.openapi.annotations.HttpMethod.POST;

class BankResource {

    private final Bank bank;

    BankResource(Bank bank) {
        this.bank = bank;
    }

    @OpenApi(
            summary = "Creates new account",
            operationId = "createAccount",
            tags = "accounts",
            path = "/accounts",
            method = POST,
            requestBody = @OpenApiRequestBody(content = @OpenApiContent(from = CreateAccountReq.class)),
            responses = {
                    @OpenApiResponse(status = "201", content = @OpenApiContent(from = AccountView.class)),
                    @OpenApiResponse(status = "400", description = "Wrong balance value")
            }
    )
    void create(Context ctx) throws InvalidMoneyValueException {
        CreateAccountReq body = ctx.bodyAsClass(CreateAccountReq.class);
        ctx.status(201).json(bank.createAccount(body.balance));
    }

    @OpenApi(
            summary = "Transfers money between accounts",
            operationId = "transfer",
            tags = "transfers",
            path = "/accounts/:id/transfers",
            method = POST,
            pathParams = @OpenApiParam(name = "id", required = true, description = "Debited account id"),
            requestBody = @OpenApiRequestBody(content = @OpenApiContent(from = TransferReq.class)),
            responses = {
                    @OpenApiResponse(status = "201", content = @OpenApiContent(from = Transfer.class)),
                    @OpenApiResponse(status = "400", description = "Transfer request invalid"),
                    @OpenApiResponse(status = "404", description = "Account not found")
            }
    )
    void transfer(Context ctx)
            throws AccountNotFoundException, InvalidMoneyValueException, NotEnoughFundsException, InvalidTransferRecipientException {

        AccountId id = accountIdParam(ctx);
        TransferReq transferReq = ctx.bodyAsClass(TransferReq.class);

        Transfer transfer = bank.transfer(id, transferReq.targetAccount, transferReq.amount);

        ctx.status(201).json(transfer);
    }

    @OpenApi(
            summary = "Get account details",
            operationId = "getAccount",
            tags = "accounts",
            path = "/accounts/:id",
            method = GET,
            pathParams = {@OpenApiParam(name = "id", required = true, description = "Account id")},
            responses = {
                    @OpenApiResponse(status = "201", content = @OpenApiContent(from = AccountView.class)),
                    @OpenApiResponse(status = "404", description = "Account not found")
            }
    )
    void get(Context ctx) throws AccountNotFoundException {
        AccountId id = accountIdParam(ctx);
        AccountView account = bank.getAccount(id);
        ctx.status(200).json(account);
    }

    @OpenApi(
            summary = "Get account transfers",
            operationId = "getTransfers",
            tags = "transfers",
            path = "/accounts/:id/transfers",
            method = GET,
            pathParams = @OpenApiParam(name = "id", required = true, description = "Account id"),
            responses = {
                    @OpenApiResponse(status = "201", content = @OpenApiContent(from = Transfer.class, isArray = true)),
                    @OpenApiResponse(status = "404", description = "Account not found")
            }
    )
    void getTransfers(Context ctx) throws AccountNotFoundException {
        AccountId id = accountIdParam(ctx);
        List<Transfer> transfers = bank.getTransfers(id);
        ctx.status(200).json(transfers);
    }

    private static AccountId accountIdParam(Context ctx) {
        return ctx.pathParam("id", AccountId.class).get();
    }
}
