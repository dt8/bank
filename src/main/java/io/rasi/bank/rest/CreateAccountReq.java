package io.rasi.bank.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.rasi.bank.Money;

public class CreateAccountReq {

    @JsonProperty
    public final Money balance;

    @JsonCreator
    public CreateAccountReq(@JsonProperty("balance") Money balance) {
        this.balance = balance;
    }
}
