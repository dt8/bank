package io.rasi.bank.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.rasi.bank.AccountId;
import io.rasi.bank.Money;

public class TransferReq {

    @JsonProperty
    public final AccountId targetAccount;
    @JsonProperty
    public final Money amount;

    @JsonCreator
    public TransferReq(@JsonProperty("targetAccount") AccountId targetAccount, @JsonProperty("amount") Money amount) {
        this.targetAccount = targetAccount;
        this.amount = amount;
    }
}
