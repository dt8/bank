package io.rasi.bank;

import io.rasi.bank.exceptions.AccountNotFoundException;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

class AccountsRepository {

    private final AtomicLong accountIdsSeq = new AtomicLong(0);
    private final ConcurrentHashMap<AccountId, Account> accounts = new ConcurrentHashMap<>();

    Account create(Money balance) {
        AccountId id = new AccountId(accountIdsSeq.incrementAndGet());
        Account account = new Account(id, balance);
        accounts.put(id, account);
        return account;
    }

    Account get(AccountId id) throws AccountNotFoundException {
        Account account = accounts.get(id);
        if (account == null) {
            throw new AccountNotFoundException();
        }
        return account;
    }
}
