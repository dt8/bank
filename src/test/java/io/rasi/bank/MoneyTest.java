package io.rasi.bank;

import org.junit.Test;

import static org.junit.Assert.*;

public class MoneyTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenInvalidStrIsPassed() {
        Money.valueOf("1.001");
    }

    @Test
    public void moneyWithSameValueShouldBeEqual() {
        assertEquals(Money.valueOf("1.01"), Money.valueOf("1.01"));
    }

    @Test
    public void shouldAddMoneyValues() {
        Money one = Money.valueOf("1.00");
        Money two = Money.valueOf("2.00");

        assertEquals(Money.valueOf("3.00"), one.add(two));
    }

    @Test
    public void shouldSubtractMoneyValues() {
        Money one = Money.valueOf("1.00");
        Money two = Money.valueOf("2.00");

        assertEquals(Money.valueOf("1.00"), two.subtract(one));
    }

    @Test
    public void shouldCompareMoneyValues() {
        Money one = Money.valueOf("1.00");
        Money two = Money.valueOf("2.00");

        assertTrue(two.greaterThat(one));
        assertFalse(one.greaterThat(two));
        assertFalse(one.greaterThat(one));
    }
}
