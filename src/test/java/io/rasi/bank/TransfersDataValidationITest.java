package io.rasi.bank;

import io.rasi.bank.rest.TransferReq;
import io.rasi.bank.testing.BankWebAppRule;
import org.junit.ClassRule;
import org.junit.Test;

import static io.rasi.bank.testing.Accounts.newAccount;
import static io.restassured.RestAssured.given;

public class TransfersDataValidationITest {

    @ClassRule
    public static BankWebAppRule bankWebAppRule = new BankWebAppRule();

    @Test
    public void shouldNotAllowForTransferringMoneyIfDebitedAccountHasNotEnoughFunds() {
        AccountId debitedAccount = newAccount(Money.valueOf("50.00")).id;
        AccountId creditedAccount = newAccount(Money.valueOf("0.00")).id;

        given()
            .contentType("application/json")
            .pathParam("id", debitedAccount.id)
            .body(new TransferReq(creditedAccount, Money.valueOf("51.00")))
        .when()
            .post("/accounts/{id}/transfers")
        .then()
            .statusCode(400);
    }

    @Test
    public void shouldNotAllowTransferringMoneyFromUnknownAccount() {
        AccountId accountId = newAccount(Money.valueOf("0.00")).id;

        given()
            .contentType("application/json")
            .pathParam("id", "143848937348")
            .body(new TransferReq(accountId, Money.valueOf("125.00")))
        .when()
            .post("/accounts/{id}/transfers")
        .then()
            .statusCode(404);
    }

    @Test
    public void shouldNotAllowTransferringMoneyToUnknownAccount() {
        AccountId accountId = newAccount(Money.valueOf("10.00")).id;

        given()
            .contentType("application/json")
            .pathParam("id", accountId.id)
            .body(new TransferReq(AccountId.valueOf("143848937348"), Money.valueOf("125.00")))
        .when()
            .post("/accounts/{id}/transfers")
        .then()
            .statusCode(400);
    }

    @Test
    public void shouldNotAllowForTransferringAmountOfZero() {
        AccountId debitedAccountId = newAccount(Money.valueOf("10.00")).id;
        AccountId creditedAccountId = newAccount(Money.valueOf("0.00")).id;

        given()
            .contentType("application/json")
            .pathParam("id", debitedAccountId.id)
            .body(new TransferReq(creditedAccountId, Money.ZERO))
        .when()
            .post("/accounts/{id}/transfers")
        .then()
            .statusCode(400);
    }

    @Test
    public void shouldNotAllowForTransferringNegativeAmount() {
        AccountId debitedAccountId = newAccount(Money.valueOf("10.00")).id;
        AccountId creditedAccountId = newAccount(Money.valueOf("0.00")).id;

        given()
            .contentType("application/json")
            .pathParam("id", debitedAccountId.id)
            .body(new TransferReq(creditedAccountId, Money.valueOf("-10.00")))
        .when()
            .post("/accounts/{id}/transfers")
        .then()
            .statusCode(400);
    }

    @Test
    public void shouldNotAllowForTransferringMoneyWithinSameAccount() {
        AccountId accountId = newAccount(Money.valueOf("10.00")).id;

        given()
            .contentType("application/json")
            .pathParam("id", accountId.id)
            .body(new TransferReq(accountId, Money.valueOf("-10.00")))
        .when()
            .post("/accounts/{id}/transfers")
        .then()
            .statusCode(400);
    }
}
