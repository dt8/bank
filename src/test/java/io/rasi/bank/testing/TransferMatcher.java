package io.rasi.bank.testing;

import io.rasi.bank.AccountId;
import io.rasi.bank.Money;
import io.rasi.bank.Transfer;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import java.util.Objects;

public class TransferMatcher extends TypeSafeMatcher<Transfer> {

    private final AccountId expectedDebitedAccount;
    private final AccountId expectedCreditedAccount;
    private final Money expectedAmount;

    private TransferMatcher(AccountId expectedDebitedAccount, AccountId expectedCreditedAccount, Money expectedAmount) {
        this.expectedDebitedAccount = expectedDebitedAccount;
        this.expectedCreditedAccount = expectedCreditedAccount;
        this.expectedAmount = expectedAmount;
    }

    public static TransferMatcher transferMatching(AccountId debitedAccount, AccountId creditedAccount, Money amount) {
        return new TransferMatcher(debitedAccount, creditedAccount, amount);
    }

    @Override
    protected boolean matchesSafely(Transfer item) {
        return Objects.equals(expectedDebitedAccount, item.debitedAccount)
                && Objects.equals(expectedCreditedAccount, item.creditedAccount)
                && Objects.equals(expectedAmount, item.amount);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("A transfer with debitedAccount: ").appendValue(expectedDebitedAccount)
                .appendText(" creditedAccount: ").appendValue(expectedCreditedAccount)
                .appendText(" amount: ").appendValue(expectedAmount);
    }
}
