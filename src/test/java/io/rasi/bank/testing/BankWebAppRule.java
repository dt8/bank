package io.rasi.bank.testing;

import io.rasi.bank.Bank;
import io.rasi.bank.rest.BankWebApp;
import io.restassured.RestAssured;
import org.junit.rules.ExternalResource;

public class BankWebAppRule extends ExternalResource {

    private final BankWebApp bankWebApp = new BankWebApp(new Bank());

    @Override
    protected void before() {
        bankWebApp.start();
        RestAssured.baseURI = "http://localhost:" + bankWebApp.port();
    }

    @Override
    protected void after() {
        bankWebApp.stop();
    }
}
