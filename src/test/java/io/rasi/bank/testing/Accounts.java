package io.rasi.bank.testing;

import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.SimpleType;
import io.rasi.bank.AccountId;
import io.rasi.bank.AccountView;
import io.rasi.bank.Money;
import io.rasi.bank.Transfer;
import io.rasi.bank.rest.CreateAccountReq;
import io.rasi.bank.rest.TransferReq;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;

public class Accounts {

    private static final CollectionType TRANSFERS_LIST_TYPE = CollectionType.construct(
            ArrayList.class,
            null,
            null,
            null,
            SimpleType.constructUnsafe(Transfer.class)
    );

    public static AccountView newAccount(Money money) {
        return given()
            .contentType("application/json")
            .body(new CreateAccountReq(money))
        .when()
            .post("/accounts")
        .then()
            .statusCode(201)
            .extract().body().as(AccountView.class);
    }

    public static Money currentBalanceOf(AccountId id) {
        AccountView account = given()
            .pathParam("id", id.id)
        .when()
            .get("/accounts/{id}")
        .then()
            .statusCode(200)
            .extract().body().as(AccountView.class);

        return account.balance;
    }

    public static Transfer transfer(AccountId debitedId, AccountId creditedId, Money amount) {
        return given()
            .contentType("application/json")
            .pathParam("id", debitedId.id)
            .body(new TransferReq(creditedId, amount))
        .when()
            .post("/accounts/{id}/transfers")
        .then()
            .statusCode(201)
            .extract().body().as(Transfer.class);
    }

    public static List<Transfer> transfersOf(AccountId id) {
        return given()
            .pathParam("id", id.id)
        .when()
            .get("/accounts/{id}/transfers")
        .then()
            .statusCode(200)
            .extract().body().as(TRANSFERS_LIST_TYPE);
    }
}
