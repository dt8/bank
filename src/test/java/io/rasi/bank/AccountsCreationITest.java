package io.rasi.bank;

import io.rasi.bank.rest.CreateAccountReq;
import io.rasi.bank.testing.BankWebAppRule;
import org.junit.ClassRule;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class AccountsCreationITest {

    @ClassRule
    public static BankWebAppRule bankWebAppRule = new BankWebAppRule();

    @Test
    public void shouldCreateAccountWithSpecifiedBalance() {
        given()
            .contentType("application/json")
            .body(new CreateAccountReq(Money.valueOf("125.00")))
        .when()
            .post("/accounts")
        .then()
            .statusCode(201)
            .body("balance", equalTo("125.00"));
    }

    @Test
    public void shouldNotAllowToCreateAccountWithNegativeBalance() {
        given()
            .contentType("application/json")
            .body(new CreateAccountReq(Money.valueOf("-1.00")))
        .when()
            .post("/accounts")
        .then()
            .statusCode(400);
    }
}
