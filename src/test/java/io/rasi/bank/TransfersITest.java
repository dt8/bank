package io.rasi.bank;

import io.rasi.bank.testing.Accounts;
import io.rasi.bank.testing.BankWebAppRule;
import io.rasi.bank.testing.TransferMatcher;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

import static io.rasi.bank.testing.Accounts.*;
import static io.rasi.bank.testing.TransferMatcher.transferMatching;
import static io.rasi.bank.Money.ZERO;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class TransfersITest {

    private static final Money ONE = Money.valueOf("1.00");
    private static final Money THREE = Money.valueOf("3.00");
    private static final Money SEVEN = Money.valueOf("7.00");
    private static final Money TEN = Money.valueOf("10.00");

    @ClassRule
    public static BankWebAppRule bankWebAppRule = new BankWebAppRule();

    @Test
    public void shouldTransferMoney() {
        // given
        AccountId debitedAccountId = newAccount(TEN).id;
        AccountId creditedAccountId = newAccount(ZERO).id;

        // when
        Transfer transfer = transfer(debitedAccountId, creditedAccountId, ONE);

        // then
        assertThat(transfer, is(transferMatching(
                debitedAccountId,
                creditedAccountId,
                ONE
        )));
    }

    @Test
    public void shouldReflectTransferAmountInBalanceForBothParties() {
        // given
        AccountId debitedAccountId = newAccount(TEN).id;
        AccountId creditedAccountId = newAccount(ZERO).id;

        // when
        transfer(debitedAccountId, creditedAccountId, SEVEN);

        // then
        assertEquals(THREE, currentBalanceOf(debitedAccountId));
        assertEquals(SEVEN, currentBalanceOf(creditedAccountId));
    }

    @Test
    public void shouldReflectTransferInTransfersHistoryForBothParties() {
        // given
        AccountId debitedAccountId = newAccount(TEN).id;
        AccountId creditedAccountId = newAccount(ZERO).id;

        // when
        transfer(debitedAccountId, creditedAccountId, SEVEN);

        // then
        List<Transfer> firstAccountTransfers = Accounts.transfersOf(debitedAccountId);
        List<Transfer> secondAccountTransfers = Accounts.transfersOf(debitedAccountId);
        TransferMatcher expectedTransfer = transferMatching(debitedAccountId, creditedAccountId, SEVEN);

        assertThat(firstAccountTransfers, hasItem(expectedTransfer));
        assertThat(secondAccountTransfers, hasItem(expectedTransfer));
    }

    @Test
    public void shouldOrderTransfersHistoryStartingFromNewest() {
        // given
        AccountId firstAccountId = newAccount(TEN).id;
        AccountId secondAccountId = newAccount(ZERO).id;

        // when
        transfer(firstAccountId, secondAccountId, THREE);
        transfer(secondAccountId, firstAccountId, ONE);

        // then
        List<Transfer> transfers = Accounts.transfersOf(firstAccountId);
        TransferMatcher firstExpectedTransfer = transferMatching(firstAccountId, secondAccountId, THREE);
        TransferMatcher secondExpectedTransfer = transferMatching(secondAccountId, firstAccountId, ONE);

        assertThat(transfers, hasSize(2));
        assertThat(transfers, contains(
                secondExpectedTransfer,
                firstExpectedTransfer
        ));
    }
}
