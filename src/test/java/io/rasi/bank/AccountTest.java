package io.rasi.bank;

import io.rasi.bank.exceptions.InvalidMoneyValueException;
import io.rasi.bank.exceptions.InvalidTransferRecipientException;
import io.rasi.bank.exceptions.NotEnoughFundsException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AccountTest {

    @Test
    public void shouldUpdateBalancesOfBothPartiesAfterTransfer() throws Exception {
        // given
        Account debitedAccount = randomAccount(Money.valueOf(10));
        Account creditedAccount = randomAccount(Money.valueOf(10));

        // when
        debitedAccount.transfer(creditedAccount, Money.valueOf(3));

        // then
        assertEquals(Money.valueOf(7), debitedAccount.balance());
        assertEquals(Money.valueOf(13), creditedAccount.balance());
    }

    @Test(expected = InvalidMoneyValueException.class)
    public void shouldNotAllowForTransferringNegativeAmounts() throws Exception {
        // given
        Account debitedAccount = randomAccount(Money.valueOf(10));
        Account creditedAccount = randomAccount();

        // when
        debitedAccount.transfer(creditedAccount, Money.valueOf("-10"));
    }

    @Test(expected = NotEnoughFundsException.class)
    public void shouldNotAllowForTransferringIfAccountHasNoEnoughFunds() throws Exception {
        // given
        Account debitedAccount = randomAccount(Money.valueOf(10));
        Account creditedAccount = randomAccount();

        // when
        debitedAccount.transfer(creditedAccount, Money.valueOf(11));
    }

    @Test(expected = InvalidTransferRecipientException.class)
    public void shouldNotAllowForTransferringToItself() throws Exception {
        // given
        Account account = randomAccount(Money.valueOf(10));

        // when
        account.transfer(account, Money.valueOf(1));
    }

    private static long accountIdsSeq = 0;

    private static Account randomAccount() {
        return randomAccount(Money.ZERO);
    }

    private static Account randomAccount(Money balance) {
        return new Account(new AccountId(++accountIdsSeq), balance);
    }
}
