# Bank app

Application has been written using Java 11.
To use it, please use `./gradlew run` and navigate to [Swagger docs](http://localhost:8080/).

#### Side notes

When the api-docs is displayed some warning messages about improperly documented endpoints
are printed to the console. This seems to be a bug in Javalin OpenAPI plugin which I reported
[here](https://github.com/tipsy/javalin/issues/787) and fixed [here](https://github.com/tipsy/javalin/pull/788).